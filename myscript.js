let className;
let href;

for (div of document.getElementsByTagName("div")) {
    className = div.className;
    href = div.querySelector("a").href;

    if (className == "link") {
        div.addEventListener("click", () => {
            document.write("Opening link on a new page!");
            window.open(href, "_blank");
        });   
    }
}

function darkMode() {
    const button = document.getElementById("darkModeButton");
    button.addEventListener("click", () => {
        document.body.classList.toggle("dark-mode");
    });
}